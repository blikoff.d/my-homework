import { createRoot } from 'react-dom/client'
import { StrictMode } from 'react'
import { Provider } from 'react-redux'

import { App } from './app'

import { store } from 'store/store'

const domNode = document.getElementById('root') as HTMLDivElement
const root = createRoot(domNode)
root.render(
  <StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </StrictMode>
)
