export interface IProductData {
  tags: Array<string>
  available: boolean
  description: string
  discount: number
  isCart: boolean
  isFavorite: boolean
  name: string
  picture: string
  pictures: string
  price: number
  stock: number
  wight: string
  _id: string
  reviews: Array<IPostData>
  likes: Array<string>
}

export type IProductsData = Array<IProductData>

export interface IPostData {
  rating: number
  image: string
  likes: Array<string>
  comments: Array<string>
  tags: Array<string>
  isPublished: boolean
  _id: string
  title: string
  author: {
    name: string
    about: string
    avatar: string
    _id: string
    email: string
    __v: number
  }
  text: string
  created_at: string
  updated_at: string
  __v: number
  updatedAt: string
}

export interface IPayloadCreatePost {
  title: string
  text: string
  image?: 'http://dummyimage.com/400x200.png/5fa2dd/ffffff'
  tags?: Array<string>
}

export interface IWidgetData {
  saleProducts: Array<IProductData>
  newProducts: Array<IProductData>
  recentlyWatchedProducts: Array<IProductData>
}
