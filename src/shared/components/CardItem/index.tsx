import clsx from 'clsx'
import { useEffect, useMemo, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import toSpliced from 'core-js-pure/actual/array/to-spliced'
import { useInView } from 'react-intersection-observer'
import { memoize } from 'proxy-memoize'

import { Label } from 'shared/components/Label'
import { setSelectedProduct } from 'store/slices/selectedProduct'
import { Price } from 'shared/components/Price'
import { MButton } from 'shared/UI/MButton'
import { useTypedSelector } from 'shared/hooks/useTypedSelector'
import { Counter } from '../Counter'

import { addRecentlyWatchedProduct, updateWidgetProduct } from 'store/slices/widgetsData'
import { useToggleLikeByParamMutation } from 'store/api/mainApi'
import { addToCart } from 'store/slices/cartProducts'
import { addToFavoriteProducts, removeFromFavoriteProducts } from 'store/slices/favoriteProducts'

import { HeartIcon, TrashIcon } from 'app/images'

import s from './CardItem.module.scss'

import { IProductData, IProductsData, IWidgetData } from 'shared/types'

export const CardItem = ({
  dataItem,
  isFull = true,
  widgetType = '',
  setCatalogData = undefined,
}: {
  dataItem: IProductData
  isFull?: boolean
  widgetType?: keyof IWidgetData | ''
  setCatalogData?: React.Dispatch<React.SetStateAction<IProductsData | null>>
}) => {
  const { ref, inView } = useInView({
    threshold: 0,
    triggerOnce: true,
  })

  const [customIsFav, setCustomIsFav] = useState(false)
  const [toggleLikeByParam] = useToggleLikeByParamMutation()

  const { user, cartProducts } = useTypedSelector(
    memoize((state) => ({
      cartProducts: state.cartProducts,
      user: state.userInfo.value,
    }))
  )

  const dispatch = useDispatch()
  const navigate = useNavigate()

  useEffect(() => {
    setCustomIsFav(dataItem.likes.includes(user.user_id))
  }, [dataItem, user])

  const { isShownCounter, count } = useMemo(() => {
    const curElem = cartProducts.find((product) => product.product._id === dataItem._id)

    return {
      isShownCounter: curElem !== undefined,
      count: curElem ? curElem.count : 0,
    }
  }, [dataItem, cartProducts])

  return (
    <div
      className={clsx(s.root, isFull && s['root-Full'])}
      onClick={() => {
        dispatch(setSelectedProduct(dataItem))
        dispatch(addRecentlyWatchedProduct(dataItem))
        navigate(`/product/${dataItem._id}`)
      }}
      ref={ref}
    >
      {dataItem.discount !== 0 && (
        <div className={s.labelWrap}>
          <Label mode="accent" text={`-${dataItem.discount}%`} />
        </div>
      )}

      {user.user_id !== '' && window.location.pathname !== '/favorites' && (
        <HeartIcon
          className={clsx(s.favoritesIcon, customIsFav && s['favoritesIcon-active'])}
          onClick={async (e) => {
            e.stopPropagation()

            if (!customIsFav) {
              try {
                const product = await toggleLikeByParam({
                  productId: dataItem._id,
                  method: 'PUT',
                }).unwrap()

                dispatch(addToFavoriteProducts(product))

                widgetType && dispatch(updateWidgetProduct({ product, type: widgetType }))
                setCatalogData &&
                  setCatalogData((prevData) => {
                    if (prevData === null) return prevData
                    const currentElemIdx = prevData?.findIndex((el) => el._id === product._id)

                    return toSpliced(prevData, currentElemIdx, 1, product)
                  })
              } catch (e) {
                console.error(e)
              }
            } else {
              try {
                const product = await toggleLikeByParam({
                  productId: dataItem._id,
                  method: 'DELETE',
                }).unwrap()

                dispatch(removeFromFavoriteProducts(product))

                widgetType && dispatch(updateWidgetProduct({ product, type: widgetType }))
                setCatalogData &&
                  setCatalogData((prevData) => {
                    if (prevData === null) return prevData
                    const currentElemIdx = prevData?.findIndex((el) => el._id === product._id)

                    return toSpliced(prevData, currentElemIdx, 1, product)
                  })
              } catch (e) {
                console.error(e)
              }
            }
          }}
        />
      )}
      {user.user_id !== '' && window.location.pathname === '/favorites' && (
        <TrashIcon
          className={clsx(s.trashIcon)}
          onClick={async (e) => {
            e.stopPropagation()

            try {
              const product = await toggleLikeByParam({
                productId: dataItem._id,
                method: 'DELETE',
              }).unwrap()

              dispatch(removeFromFavoriteProducts(product))
            } catch (e) {
              console.error(e)
            }
          }}
        />
      )}

      <div className={clsx(s.photo, isFull && s['photo-Full'])}>
        <img src={inView ? dataItem.pictures : ''} />
      </div>
      <div className={s.info}>
        <Price dataItem={dataItem} />
        <div className={s.quantity}>{dataItem.wight}</div>
        <div className={s.description} title={dataItem.name}>
          {dataItem.name}
        </div>
      </div>
      {isFull && !isShownCounter && (
        <MButton
          onClick={(e) => {
            e.stopPropagation()
            dispatch(addToCart({ product: dataItem, count: 1 }))
          }}
        >
          В корзину
        </MButton>
      )}
      {isShownCounter && isFull && (
        <div onClick={(e) => e.stopPropagation()}>
          <Counter inlineStyles={{ height: 36 }} product={{ product: dataItem, count }} />
        </div>
      )}
    </div>
  )
}
