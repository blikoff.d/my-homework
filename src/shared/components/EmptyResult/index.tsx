import { useNavigate } from 'react-router-dom'

import { SadSmileIcon } from 'app/images'

import { MButton } from 'shared/UI/MButton'

import s from './EmptyResult.module.scss'
import { EmptyResultProps } from './types'

export const EmptyResult: React.FC<EmptyResultProps> = ({ title, subtitle }: EmptyResultProps) => {
  const navigate = useNavigate()

  return (
    <div className={s.root}>
      <SadSmileIcon />
      <div className={s.text}>
        {title && <div className={s.title}>{title}</div>}
        {subtitle && <div className={s.subtitle}>{subtitle}</div>}
      </div>
      <MButton outlined onClick={() => navigate('/')}>
        На главную
      </MButton>
    </div>
  )
}
