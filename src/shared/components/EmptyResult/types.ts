export interface EmptyResultProps {
  title?: string
  subtitle?: string
}
