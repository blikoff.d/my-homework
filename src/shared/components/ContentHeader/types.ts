import { IProductData } from 'shared/types'

export interface ContentHeaderProps {
  title: string
  navText?: string | number
  inlineStyles?: { [t: string]: string | number }
  product?: IProductData | null
}
