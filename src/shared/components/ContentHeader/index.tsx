import { useNavigate } from 'react-router-dom'
import { useMemo } from 'react'
import { Rating } from '@mui/material'

import { BackArrowIcon } from 'app/images'

import s from './ContentHeader.module.scss'

import { ContentHeaderProps } from './types'

import resolveCountRewievsTitle from 'shared/helpers/resolveCountName'

export const ContentHeader: React.FC<ContentHeaderProps> = ({
  navText = 'Назад',
  title,
  product = null,
  inlineStyles = {},
}: ContentHeaderProps) => {
  const navigate = useNavigate()

  const rating = useMemo(() => {
    if (!product) return 0
    return product.reviews.reduce((acc, curr) => (acc += curr.rating), 0) / product.reviews.length
  }, [product])

  return (
    <div className={s.root} style={inlineStyles}>
      <div className={s.nav} onClick={() => (navText === 'Назад' ? navigate(-2) : navigate('/'))}>
        <BackArrowIcon className={s.backArrow} /> {navText}
      </div>
      <div className={s.title}>{title}</div>
      {product && (
        <div className={s.additionalRow}>
          <div className={s.vendorCodeWrap}>
            Артикул: <span className={s.vendorCode}>{product._id}</span>
          </div>
          <div className={s.reviewsWrap}>
            <Rating name="rating" value={rating} precision={0.5} readOnly />

            <div
              className={s.reviews}
              onClick={() => {
                const reviewsEl = document.getElementById('reviews')
                reviewsEl?.scrollIntoView({ behavior: 'smooth', block: 'start' })
              }}
            >{`${product.reviews.length} ${resolveCountRewievsTitle(product.reviews.length, {
              noMany: 'отзывов',
              one: 'отзыв',
              noOne: 'отзыва',
            })}`}</div>
          </div>
        </div>
      )}
    </div>
  )
}
