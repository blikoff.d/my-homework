import clsx from 'clsx'

import s from './Label.module.scss'

export const Label = ({ text, mode }: { text: string; mode: 'blue' | 'accent' | 'green' }) => {
  return <div className={clsx(s.root, s[mode])}>{text}</div>
}
