import s from './Loader.module.scss'

export const Loader = () => {
  return (
    <div className={s.root}>
      <div className={s['lds-facebook']}>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  )
}
