import clsx from 'clsx'
import s from './Banner.module.scss'
import { BannerProps } from './types'

export const Banner: React.FC<BannerProps> = ({ children, size, color }: BannerProps) => {
  return <div className={clsx(s.root, s[`root-${size}`], s[`root-color${color}`])}>{children}</div>
}
