export interface BannerProps {
  size: 'md' | 'lg'
  children: React.ReactNode
  color: 'Orange' | 'Brown' | 'Ocean'
}
