import clsx from 'clsx'
import s from './CounterIcon.module.scss'

export const CounterIcon = ({ num = 0 }: { num?: number }) => {
  return (
    <div className={clsx(s.root)}>
      <div className={s.container}>{num}</div>
    </div>
  )
}
