import clsx from 'clsx'
import { useDispatch } from 'react-redux'
import { memoize } from 'proxy-memoize'

import s from './Counter.module.scss'

import { MinusIcon, PlusIcon } from 'app/images'
import { IProductData } from 'shared/types'

import { addToCart, removeFromCart } from 'store/slices/cartProducts'
import { useTypedSelector } from 'shared/hooks/useTypedSelector'

export const Counter = ({
  product,
  inlineStyles,
}: {
  product: {
    product: IProductData
    count: number
  }
  inlineStyles?: {
    [t: string]: string | number
  }
}) => {
  const count =
    useTypedSelector(
      memoize(
        (state) =>
          state.cartProducts.find((prod) => prod.product._id === product.product._id)?.count
      )
    ) || 0

  const dispatch = useDispatch()

  return (
    <div className={s.root} style={{ ...inlineStyles }}>
      <MinusIcon
        className={clsx(s.minus, count === 0 && s['minus-disabled'])}
        onClick={() => {
          if (count === 0) return
          dispatch(removeFromCart(product))
        }}
      />

      <div className={s.count}>{count}</div>

      <PlusIcon className={s.plus} onClick={() => dispatch(addToCart(product))} />
    </div>
  )
}
