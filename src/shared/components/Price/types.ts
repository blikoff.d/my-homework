import { IProductData } from 'shared/types'

export interface PriceProps {
  dataItem: IProductData
}
