import clsx from 'clsx'

import s from './Price.module.scss'
import { PriceProps } from './types'

export const Price: React.FC<PriceProps> = ({ dataItem }: PriceProps) => {
  const options = { style: 'currency', currency: 'RUB', maximumFractionDigits: 0 }
  const numberFormat = new Intl.NumberFormat('ru-RU', options)

  return (
    <div className={s.root}>
      <div className={clsx(s.price_old, dataItem.discount === 0 && s['price_old-hidden'])}>
        {numberFormat.format(
          Math.round(dataItem.price + (dataItem.price * dataItem.discount) / 100)
        )}
      </div>

      <div className={clsx(s.price, dataItem.discount !== 0 && s['price-discount'])}>
        {numberFormat.format(dataItem.price)}
      </div>
    </div>
  )
}
