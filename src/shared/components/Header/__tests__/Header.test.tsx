import '@testing-library/jest-dom'
import { screen, waitFor } from '@testing-library/react'

import { setupTestsWrapper } from 'shared/helpers/setupTestWrapper'

describe('Header tests', () => {
  setupTestsWrapper({ initialRouterEntries: ['/'] })

  describe('Check elements in DOM tree', () => {
    test('Header logo must be in the document', () => {
      waitFor(() => expect(screen.findByTestId('header-mainLogo')).toBeInTheDocument())
    })
    test('Header cart must be in the document', () => {
      waitFor(() => expect(screen.findByTestId('header-cart')).toBeInTheDocument())
    })
    test('Header favorites must be in the document', () => {
      waitFor(() => expect(screen.findByTestId('header-favs')).toBeInTheDocument())
    })
    test('Header login icon must be in the document', () => {
      waitFor(() => expect(screen.findByTestId('header-login')).toBeInTheDocument())
    })
  })
})
