import clsx from 'clsx'
import { useEffect, useMemo, useRef, useState } from 'react'
import { debounce } from 'lodash'
import { useNavigate, useSearchParams } from 'react-router-dom'

import { useTypedSelector } from 'shared/hooks/useTypedSelector'

import { CloseIcon, LoopIcon } from 'app/images'

import s from './SearchInput.module.scss'
import { PRODUCTS_SEARCH_PARAMS_KEY } from 'shared/constants'
import { isUserLoggedInSelector } from 'store/selectors'

export const SearchInput = () => {
  const navigate = useNavigate()
  const [searchParams, setSearchParams] = useSearchParams()
  const [inputValue, setInputValue] = useState('')

  const isUserLoggedIn = useTypedSelector(isUserLoggedInSelector)

  const inputRef = useRef<HTMLInputElement>(null)

  const [, searchedQueryValue] = useMemo(() => {
    const params: Array<[string, string]> = []
    searchParams.forEach((value, key) => {
      params.push([key, value])
    })

    const searchedQuery = params.find((el) => el[0] === PRODUCTS_SEARCH_PARAMS_KEY) || ['', '']
    return searchedQuery
  }, [searchParams.toString()])

  useEffect(() => {
    setInputValue(searchedQueryValue)
  }, [])

  useEffect(() => {
    if (location.pathname === '/catalog') return

    clearHandler()
  }, [location.pathname])

  const debouncedFn = useMemo(
    () =>
      debounce((e: React.ChangeEvent<HTMLInputElement>) => {
        const valueTrimmed = e.target.value
        if (valueTrimmed) {
          searchParams.set(PRODUCTS_SEARCH_PARAMS_KEY, valueTrimmed)
        } else {
          searchParams.delete(PRODUCTS_SEARCH_PARAMS_KEY)
        }
        setSearchParams(searchParams)
      }, 1000),
    []
  )

  useEffect(() => {
    return debouncedFn.cancel()
  }, [])

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (location.pathname !== '/catalog') {
      navigate('/catalog')
    }

    const valueTrimmed = e.target.value

    setInputValue(valueTrimmed)
    debouncedFn(e)
  }

  const clearHandler = () => {
    setInputValue('')
    searchParams.delete(PRODUCTS_SEARCH_PARAMS_KEY)
    setSearchParams(searchParams)
  }

  return (
    <div className={s.inputWrap}>
      <input
        className={s.input}
        onChange={handleInputChange}
        value={inputValue}
        placeholder="Поиск"
        ref={inputRef}
        disabled={!isUserLoggedIn}
      />
      <CloseIcon
        className={clsx(s.closeIcon, !inputValue && s['closeIcon-hidden'])}
        onClick={clearHandler}
      />
      <LoopIcon
        className={clsx(s.loopIcon, inputValue && s['loopIcon-hidden'])}
        onClick={() => inputRef.current?.focus()}
      />
    </div>
  )
}
