import clsx from 'clsx'
import { Link, useNavigate } from 'react-router-dom'
import { memoize } from 'proxy-memoize'

import { SearchInput } from './SearchInput'

import { useTypedSelector } from 'shared/hooks/useTypedSelector'

import { CartIcon, DogIcon, HeartIcon, MainLogoIcon } from 'app/images'

import s from './Header.module.scss'
import { CounterIcon } from '../CounterIcon/CounterIcon'
import { cartProductsCountSelector } from 'store/selectors'

export const Header = () => {
  const navigate = useNavigate()

  const favoriteProducts = useTypedSelector(memoize((state) => state.favoriteProducts))
  const cartCount = useTypedSelector(cartProductsCountSelector)

  return (
    <div className={clsx('container', s.root)}>
      <MainLogoIcon
        className={s.logo}
        onClick={() => navigate('/')}
        data-testid="header-mainLogo"
      />

      <SearchInput />

      <div className={s.menu} data-testid="header-menu">
        <Link to="/favorites">
          <div className={s.iconWrap} data-testid="header-favs">
            <HeartIcon className={s.menu_icon} />
            <CounterIcon num={favoriteProducts.length} />
          </div>
        </Link>
        <Link to="/cart">
          <div className={s.iconWrap} data-testid="header-cart">
            <CartIcon className={s.menu_icon} />
            <CounterIcon num={cartCount} />
          </div>
        </Link>

        <DogIcon
          className={s.menu_icon}
          onClick={() => navigate('/profile')}
          data-testid="header-login"
        />
      </div>
    </div>
  )
}
