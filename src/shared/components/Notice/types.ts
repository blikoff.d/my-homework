export type NoticeProps = {
  text: string
  isShown: boolean
  setIsShown: (s: boolean) => void
}
