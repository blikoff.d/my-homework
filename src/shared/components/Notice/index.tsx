import clsx from 'clsx'
import { useDispatch } from 'react-redux'
import { memoize } from 'proxy-memoize'

import { CloseIcon } from 'app/images'

import { useTypedSelector } from 'shared/hooks/useTypedSelector'
import { setActiveNotices } from 'store/slices/activeNotices'

import s from './Notice.module.scss'
import { useEffect } from 'react'
import { RootState } from 'store/store'

export const Notice = () => {
  const dispatch = useDispatch()
  const activeNotices = useTypedSelector(memoize((state: RootState) => state.activeNotices))

  useEffect(() => {
    const timeoutFn = setTimeout(() => {
      dispatch(setActiveNotices(''))
    }, 7000)

    return () => clearTimeout(timeoutFn)
  }, [])

  return (
    <div className={clsx(s.root, activeNotices === '' && s['root-hidden'])}>
      <div className={s.container}>
        {activeNotices}
        <CloseIcon className={s.root_closeIcon} onClick={() => dispatch(setActiveNotices(''))} />
      </div>
    </div>
  )
}
