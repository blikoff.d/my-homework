import clsx from 'clsx'

import { Link, useNavigate } from 'react-router-dom'

import s from './Footer.module.scss'
import { MainLogoIcon } from 'app/images'

const menu = {
  left: [
    { text: 'Каталог', path: '/catalog', soon: false },
    { text: 'Акции', path: '/sales', soon: true },
    { text: 'Новости', path: '/news', soon: true },
    { text: 'Отзывы', path: '/reviews', soon: true },
  ],

  right: [
    { text: 'Оплата и доставка', path: '/payments', soon: true },
    { text: 'Часто спрашивают', path: '/faq', soon: false },
    { text: 'Обратная связь', path: '/feedback', soon: true },
    { text: 'Контакты', path: '/contacts', soon: true },
  ],
}

export const Footer = () => {
  const navigate = useNavigate()

  return (
    <div className={clsx(s.root, 'container', 'footer')} data-testid="footer">
      <div className={s.root_wrap}>
        <div className={s.logoWrap} onClick={() => navigate('/')}>
          <MainLogoIcon data-testid="footer-mainLogo" />
          <div className={s.copyright}>© «Интернет-магазин DogFood.ru»</div>
        </div>

        <div className={s.menu}>
          {Object.keys(menu).map((menuSection) => (
            <div key={menuSection}>
              {menu[menuSection as keyof typeof menu].map((menuItem) => {
                if (!menuItem.soon) {
                  return (
                    <Link
                      to={menuItem.path}
                      key={menuItem.text}
                      data-testid={`footer-link-to-${menuItem.path}`}
                    >
                      {menuItem.text}
                    </Link>
                  )
                }

                return (
                  <div className={s.soonNavItem} key={menuItem.text}>
                    {menuItem.text}
                    <div className={s.label}>Скоро</div>
                  </div>
                )
              })}
            </div>
          ))}
        </div>

        <div className={s.contacts} data-testid="footer-contacts">
          <div className={s.contacts_title}>Мы на связи</div>
          <div className={s.contacts_info}>
            <div className={s.number}>8 (999) 00-00-00</div>
            <a
              className={s.mail}
              href="mailto: dogfood.ru@gmail.com"
              target="_blank"
              rel="noreferrer"
            >
              dogfood.ru@gmail.com
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}
