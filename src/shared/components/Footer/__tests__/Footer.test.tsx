import '@testing-library/jest-dom'
import { screen, waitFor } from '@testing-library/react'

import { setupTestsWrapper } from 'shared/helpers/setupTestWrapper'

const menu = [
  '/catalog',
  '/sales',
  '/news',
  '/reviews',
  '/payments',
  '/faq',
  '/feedback',
  '/contacts',
]

describe('Footer tests', () => {
  setupTestsWrapper({ initialRouterEntries: ['/'] })

  describe('Check elements in DOM tree', () => {
    test('Footer must be in the document', () => {
      waitFor(() => expect(screen.findByTestId('footer')).toBeInTheDocument())
    })
    test('Footer main logo must be in the document', () => {
      waitFor(() => expect(screen.findByTestId('footer-mainLogo')).toBeInTheDocument())
    })
    test('Footer contacts must be in the document', () => {
      waitFor(() => expect(screen.findByTestId('footer-contacts')).toBeInTheDocument())
    })
    test('Footer links must be in the document', () => {
      waitFor(() => {
        menu.map((el) => {
          expect(screen.findByTestId(`footer-link-to-${el}`)).toBeInTheDocument()
        })
      })
    })
  })
})
