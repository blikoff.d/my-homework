import clsx from 'clsx'
import { useDispatch } from 'react-redux'
import { useEffect, useState } from 'react'
import { useLocation, useNavigate, Outlet } from 'react-router-dom'

import { Footer } from 'shared/components/Footer'
import { Header } from 'shared/components/Header'
import { Notice } from 'shared/components/Notice'

import { MModal } from 'shared/UI/MModal'
import { MButton } from 'shared/UI/MButton'

import { SignIn } from 'shared/modals/SignIn'
import { ResetPass } from 'shared/modals/ResetPass'
import { SignUp } from 'shared/modals/SignUp'

import { useTypedSelector } from 'shared/hooks/useTypedSelector'

import { setActiveModals } from 'store/slices/activeModals'
import { setInitialFavs } from 'store/slices/favoriteProducts'

import { useGetProductsForWidgestsQuery } from 'store/api/mainApi'

import { MainPageDogIcon, NextArrowStrokedIcon } from 'app/images'

import s from './MainLayout.module.scss'
import { categories } from './constants'
import { mainLayoutSelector } from 'store/selectors'

export const MainLayout = () => {
  const { activeModals, userId, isUserLoggedIn } = useTypedSelector(mainLayoutSelector)
  const [skip, setSkip] = useState(true)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const location = useLocation()

  const { data, isLoading } = useGetProductsForWidgestsQuery(null, {
    skip,
  })

  useEffect(() => {
    if (isUserLoggedIn) {
      setSkip(false)
    }
  }, [isUserLoggedIn])

  useEffect(() => {
    if (data === undefined) {
      dispatch(setInitialFavs([]))
      return
    }

    const favorites = data.products.filter((product) => product.likes.includes(userId))

    dispatch(setInitialFavs(favorites))
  }, [isLoading, isUserLoggedIn])

  return (
    <div className="wrapper">
      <MModal
        isOpen={activeModals.includes('signIn')}
        onClose={() => {
          navigate('/')
          dispatch(setActiveModals([]))
        }}
      >
        <SignIn />
      </MModal>
      <MModal
        isOpen={activeModals.includes('resetPass')}
        onClose={() => {
          navigate('/')
          dispatch(setActiveModals([]))
        }}
      >
        <ResetPass />
      </MModal>
      <MModal
        isOpen={activeModals.includes('signUp')}
        onClose={() => {
          navigate('/')
          dispatch(setActiveModals([]))
        }}
      >
        <SignUp />
      </MModal>

      <Notice />

      <Header />
      {location.pathname === '/' ? (
        <div className={s.angleBackgroundWrap}>
          <div className={clsx('container', s.innerWrap)}>
            <div className={s.mainBanner}>
              <div className={s.contentBlock}>
                <div className={s.title}>Крафтовые лакомства для собак</div>
                <div className={s.subtitle}>
                  Всегда свежие лакомства ручной работы с доставкой по России и Миру
                </div>
                <MButton
                  white
                  inlineStyles={{ font: 'var(--font-h3)' }}
                  onClick={() => navigate('/catalog')}
                >
                  Каталог <NextArrowStrokedIcon style={{ marginLeft: 8 }} />
                </MButton>
              </div>

              <MainPageDogIcon className={s.mainPageDogIcon} />
            </div>
            <div className={s.categories}>
              {categories.map((category) => (
                <div className={s.card} key={category.name}>
                  {category.component}
                  {category.name}
                </div>
              ))}
            </div>
          </div>
        </div>
      ) : null}

      <div className="content">
        <div className={clsx('container', s.container)}>
          <Outlet />
        </div>
      </div>
      <Footer />
    </div>
  )
}
