import {
  CategoryAccessoryIcon,
  CategoryGoodiesIcon,
  CategoryHornsIcon,
  CategoryOilsIcon,
  CategorySetIcon,
  CategoryToysIcon,
} from 'app/images'

export const categories = [
  {
    name: 'Наборы',
    component: <CategorySetIcon />,
  },
  {
    name: 'Лакомства',
    component: <CategoryGoodiesIcon />,
  },
  {
    name: 'Аксессуары',
    component: <CategoryAccessoryIcon />,
  },
  {
    name: 'Игрушки',
    component: <CategoryToysIcon />,
  },
  {
    name: 'Рога',
    component: <CategoryHornsIcon />,
  },
  {
    name: 'Масла',
    component: <CategoryOilsIcon />,
  },
]
