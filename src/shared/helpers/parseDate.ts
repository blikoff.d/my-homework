export const parseDate = (dateString: string) => {
  const months = {
    0: 'янв',
    1: 'фев',
    2: 'мар',
    3: 'апр',
    4: 'май',
    5: 'июн',
    6: 'июл',
    7: 'авг',
    8: 'сент',
    9: 'окт',
    10: 'ноя',
    11: 'дек',
  }

  const currentDate = new Date(Date.parse(dateString))

  const year = currentDate.getFullYear()
  const day = currentDate.getDate()
  const month = months[currentDate.getMonth()]

  return `${day} ${month} ${year}`
}
