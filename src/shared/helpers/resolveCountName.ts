const resolveCountTitle = (
  count: number,
  wordForms: { noMany: string; one: string; noOne: string }
) => {
  const stringifiedCount = String(count)
  const lastNum = +stringifiedCount[stringifiedCount.length - 1]

  const last2NumbersInNumeric = +`${stringifiedCount[stringifiedCount.length - 2]}${
    stringifiedCount[stringifiedCount.length - 1]
  }`

  switch (true) {
    case lastNum === 0 || lastNum > 4 || (last2NumbersInNumeric > 10 && last2NumbersInNumeric < 20):
      return wordForms.noMany
    case lastNum === 1:
      return wordForms.one
    default:
      return wordForms.noOne
  }
}

export default resolveCountTitle
