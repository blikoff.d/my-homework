import resolveCountName from '../resolveCountName'

describe('Тестируем хелперы проекта', () => {
  describe('Тестируем хелпер resolveCountName', () => {
    const testMock = {
      wordForms: { noMany: 'рублей', one: 'рубль', noOne: 'рубля' },
    }

    test('resolveCountName проверка чисел с окончанием на 1', () => {
      expect(resolveCountName(21, testMock.wordForms)).toEqual('рубль')
      expect(resolveCountName(1, testMock.wordForms)).not.toEqual('рублей')
      expect(resolveCountName(31, testMock.wordForms)).not.toEqual('рублей')
    })

    test('resolveCountName проверка чисел больше 4 с окончанием не на 1', () => {
      expect(resolveCountName(20, testMock.wordForms)).toEqual('рублей')
      expect(resolveCountName(25, testMock.wordForms)).toEqual('рублей')
      expect(resolveCountName(12, testMock.wordForms)).toEqual('рублей')
      expect(resolveCountName(12, testMock.wordForms)).not.toEqual('рубля')
    })

    test('resolveCountName проверка чисел исключений', () => {
      expect(resolveCountName(122, testMock.wordForms)).toEqual('рубля')
      expect(resolveCountName(2, testMock.wordForms)).toEqual('рубля')
      expect(resolveCountName(2, testMock.wordForms)).not.toEqual('рублей')
    })
  })
})
