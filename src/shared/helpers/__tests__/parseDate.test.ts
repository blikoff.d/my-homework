import { parseDate } from '../parseDate'

describe('Тестируем хелпер parseDate', () => {
  test('parseDate 2011-08-12T20:17:46.384Z', () => {
    expect(parseDate('2011-08-12T20:17:46.384Z')).toEqual('13 авг 2011')
  })

  test('parseDate 2015-03-22T20:17:46.384Z', () => {
    expect(parseDate('2015-03-22T20:17:46.384Z')).toEqual('22 мар 2015')
  })

  test('parseDate 2023-01-02T20:17:46.384Z', () => {
    expect(parseDate('2023-01-02T20:17:46.384Z')).toEqual('2 янв 2023')
  })

  test('parseDate 2021-01-02T20:17:46.384Z', () => {
    expect(parseDate('2021-01-02T20:17:46.384Z')).toEqual('2 янв 2021')
  })

  test('parseDate 2001-12-31T20:17:46.384Z', () => {
    expect(parseDate('2001-12-31T20:17:46.384Z')).toEqual('31 дек 2001')
  })

  test('parseDate 2001-12-31T20:17:46.384Z', () => {
    expect(parseDate('2001-12-31T20:17:46.384Z')).not.toEqual('30 дек 2001')
  })
})
