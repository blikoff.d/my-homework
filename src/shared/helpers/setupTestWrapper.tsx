import { render } from '@testing-library/react'

import { Provider } from 'react-redux'
import { createMemoryRouter } from 'react-router-dom'
import { RouterProvider } from 'react-router'

import { routerConfig } from 'app/routerConfig'
import { store } from 'store/store'

interface WrapperForTestsProps {
  initialRouterEntries: string[]
}

export const setupTestsWrapper = ({ initialRouterEntries }: WrapperForTestsProps) => {
  const router = createMemoryRouter(routerConfig, {
    initialEntries: initialRouterEntries,
  })

  render(
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  )

  return router
}
