import * as yup from 'yup'

export const schemaEmail = yup
  .object({
    email: yup.string().email().required(),
  })
  .required()

export type FormDataEmailType = yup.InferType<typeof schemaEmail>

export const schema = yup
  .object({
    tokenFromMail: yup.string().required(),
    password: yup.string().min(6).max(24).required(),
  })
  .required()

export type FormDataType = yup.InferType<typeof schema>
