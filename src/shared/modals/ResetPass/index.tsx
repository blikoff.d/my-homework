import { useForm } from 'react-hook-form'
import { useDispatch } from 'react-redux'
import { useState } from 'react'
import { yupResolver } from '@hookform/resolvers/yup'

import { MButton } from 'shared/UI/MButton'

import { setActiveModals } from 'store/slices/activeModals'
import { setActiveNotices } from 'store/slices/activeNotices'

import { BackArrowIcon } from 'app/images'

import { FormDataEmailType, FormDataType, schema, schemaEmail } from './schemas'

import s from './ResetPass.module.scss'
import clsx from 'clsx'
import { useForgotPasswordMutation, useResetPasswordMutation } from 'store/api/mainApi'

export const ResetPass = () => {
  const [isReadyForInputNewPass, setIsReadyForInputNewPass] = useState(false)
  const [forgotPasswordByEmail] = useForgotPasswordMutation()
  const [resetPasswordByEmail] = useResetPasswordMutation()
  const dispatch = useDispatch()

  const {
    register: registerEmail,
    handleSubmit: handleSubmitEmail,
    formState: { errors: errorsEmail, isSubmitting: isSubmittingEmail, isValid: isValidEmail },
    reset: resetEmail,
  } = useForm({
    resolver: yupResolver(schemaEmail),
    defaultValues: {
      email: '',
    },
  })

  const {
    register,
    handleSubmit,
    formState: { errors, isValid, isSubmitting },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      tokenFromMail: '',
      password: '',
    },
  })

  const onSubmit = async (dataFromForm: FormDataType) => {
    try {
      const response = await resetPasswordByEmail(dataFromForm).unwrap()

      if (response) {
        reset()
        dispatch(setActiveModals(['signIn']))
        dispatch(setActiveNotices('Пароль успешно изменен!'))
      }
    } catch (e) {
      dispatch(setActiveNotices('Что-то пошло не так! Обратитесь к администратору.'))
    }
  }

  const onSubmitSendCodeToEmail = async (dataFromForm: FormDataEmailType) => {
    try {
      const response = await forgotPasswordByEmail(dataFromForm).unwrap()

      if (response) {
        resetEmail()
        setIsReadyForInputNewPass(true)
        dispatch(
          setActiveNotices(
            'На указанный адрес отправлено письмо с кодом, вставьте его в форму ниже.'
          )
        )
      }
    } catch (e) {
      dispatch(setActiveNotices('Что-то пошло не так! Обратитесь к администратору.'))
    }
  }

  return (
    <div className={s.root}>
      <div className={s.title}>
        <div
          className={s.back}
          onClick={() => {
            if (isReadyForInputNewPass) {
              reset()
              setIsReadyForInputNewPass(false)
              return
            }

            dispatch(setActiveModals(['signIn']))
          }}
        >
          <BackArrowIcon className={s.backArrowIcon} />
          Назад
        </div>
        Восстановление пароля
      </div>
      {isReadyForInputNewPass ? (
        <form className={s.form} onSubmit={handleSubmit(onSubmit)}>
          <div className={s.text}>Введите код, который пришел вам на почту и новый пароль.</div>
          <input
            className={clsx(s.input, errors.tokenFromMail?.message && s['input-failed'])}
            placeholder="Секретный код"
            {...register('tokenFromMail')}
          />
          <p className={s.invalidInputText}>{errors.tokenFromMail?.message}</p>
          <input
            className={clsx(s.input, errors.password?.message && s['input-failed'])}
            placeholder="Новый пароль"
            type="password"
            {...register('password')}
          />
          <p className={s.invalidInputText}>{errors.password?.message}</p>
          <MButton
            type="submit"
            inlineStyles={{ width: '100%' }}
            disabled={isSubmitting || !isValid}
          >
            Отправить
          </MButton>
        </form>
      ) : (
        <form className={s.form} onSubmit={handleSubmitEmail(onSubmitSendCodeToEmail)}>
          <div className={s.text}>
            Для получения временного пароля необходимо ввести email, указанный при регистрации.
          </div>
          <input
            className={clsx(s.input, errorsEmail.email?.message && s['input-failed'])}
            placeholder="Email"
            type="mail"
            {...registerEmail('email')}
          />
          <p className={s.invalidInputText}>{errorsEmail.email?.message}</p>
          <div className={s.text}>Срок действия временного пароля 24 ч.</div>
          <MButton
            inlineStyles={{ width: '100%' }}
            type="submit"
            disabled={isSubmittingEmail || !isValidEmail}
          >
            Отправить
          </MButton>

          <MButton
            inlineStyles={{ width: '100%' }}
            onClick={() => setIsReadyForInputNewPass(true)}
            outlined
          >
            У меня есть токен
          </MButton>
        </form>
      )}
    </div>
  )
}
