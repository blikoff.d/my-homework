import * as yup from 'yup'

export const schema = yup
  .object({
    email: yup.string().email().required(),
    password: yup.string().min(6).max(24).required(),
    group: yup.string().required(),
  })
  .required()

export type FormDataType = yup.InferType<typeof schema>
