import { useDispatch } from 'react-redux'
import clsx from 'clsx'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import { MButton } from 'shared/UI/MButton'

import s from './SignUp.module.scss'

import { setActiveModals } from 'store/slices/activeModals'

import { FormDataType, schema } from './schema'
import { useSignUpMutation } from 'store/api/mainApi'
import { setActiveNotices } from 'store/slices/activeNotices'

export const SignUp = () => {
  const dispatch = useDispatch()
  const [signUp] = useSignUpMutation()

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting, isValid },
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      group: 'group-11',
    },
  })

  const onSubmit = async (dataFromForm: FormDataType) => {
    try {
      const response = await signUp({ ...dataFromForm }).unwrap()

      if (response) {
        console.warn(response)

        dispatch(setActiveModals(['signIn']))
        dispatch(setActiveNotices('Вы успешно зарегистрировались!'))
      }
    } catch (e) {
      console.error(e)
      if ((e as any).data.message === 'Пользователь с данным email уже существует') {
        dispatch(setActiveNotices('Пользователь с таким email уже зарегистрирован в системе.'))
        return
      }

      dispatch(setActiveNotices('Что-то пошло не так! Обратитесь к администратору.'))
    }
  }

  return (
    <div className={s.root}>
      <div className={s.title}>Регистрация</div>

      <form className={s.form} onSubmit={handleSubmit(onSubmit)}>
        <div className={s.inputGroup}>
          <input
            className={clsx(s.input, errors.group?.message && s['input-failed'])}
            placeholder="Group"
            value="group-11"
            {...(register('group'),
            {
              disabled: true,
            })}
          />
          <p className={s.invalidInputText}>{errors.group?.message}</p>
          <input
            className={clsx(s.input, errors.email?.message && s['input-failed'])}
            placeholder="Email"
            {...register('email')}
          />
          <p className={s.invalidInputText}>{errors.email?.message}</p>
          <input
            className={clsx(s.input, errors.password?.message && s['input-failed'])}
            placeholder="Пароль"
            type="password"
            {...register('password')}
          />
          <p className={s.invalidInputText}>{errors.password?.message}</p>
        </div>
        <div className={s.text}>
          Регистрируясь на сайте, вы соглашаетесь с нашими Правилами и Политикой конфиденциальности
          и соглашаетесь на информационную рассылку.
        </div>
        <div className={s.buttonGroup}>
          <MButton
            inlineStyles={{ width: '100%' }}
            type="submit"
            disabled={isSubmitting || !isValid}
          >
            Зарегистрироваться
          </MButton>
          <MButton
            outlined
            inlineStyles={{ width: '100%' }}
            onClick={() => dispatch(setActiveModals(['signIn']))}
          >
            Войти
          </MButton>
        </div>
      </form>
    </div>
  )
}
