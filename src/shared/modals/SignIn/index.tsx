import { useDispatch } from 'react-redux'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import clsx from 'clsx'

import { MButton } from 'shared/UI/MButton'

import { setActiveModals } from 'store/slices/activeModals'
import { setUserInfo } from 'store/slices/userInfo'
import { useSignInMutation } from 'store/api/mainApi'

import s from './SignIn.module.scss'
import { schema, FormDataType } from './schema'

export const SignIn = () => {
  const [signIn] = useSignInMutation()
  const dispatch = useDispatch()

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting, isValid },
  } = useForm({
    resolver: yupResolver(schema),
  })

  const onSubmit = async (dataFromForm: FormDataType) => {
    try {
      const { data, token } = await signIn(dataFromForm).unwrap()

      if (data) {
        console.warn(data, token)
        dispatch(
          setUserInfo({
            user_name: data.name,
            user_email: data.email,
            user_about: data.about,
            user_id: data._id,
            user_token: token,
          })
        )
        dispatch(setActiveModals([]))
      }
    } catch (e) {
      console.error(e)
    }
  }

  return (
    <div className={s.root}>
      <div className={s.title}>Вход</div>

      <form className={s.form} onSubmit={handleSubmit(onSubmit)}>
        <div className={s.inputGroup}>
          <input
            className={clsx(s.input, errors.email?.message && s['input-failed'])}
            placeholder="Email"
            {...register('email')}
          />
          <p className={s.invalidInputText}>{errors.email?.message}</p>
          <input
            className={clsx(s.input, errors.password?.message && s['input-failed'])}
            placeholder="Пароль"
            type="password"
            {...register('password')}
          />
          <p className={s.invalidInputText}>{errors.password?.message}</p>
          <div className={s.resetPass} onClick={() => dispatch(setActiveModals(['resetPass']))}>
            Восстановить пароль
          </div>
        </div>
        <div className={s.buttonGroup}>
          <MButton
            inlineStyles={{ width: '100%' }}
            type="submit"
            disabled={isSubmitting || !isValid}
          >
            Войти
          </MButton>
          <MButton
            outlined
            inlineStyles={{ width: '100%' }}
            onClick={() => dispatch(setActiveModals(['signUp']))}
          >
            Регистрация
          </MButton>
        </div>
      </form>
    </div>
  )
}
