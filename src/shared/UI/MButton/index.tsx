import { Button } from '@mui/material'

import { MButtonProps } from './types'

export const MButton: React.FC<MButtonProps> = ({
  inlineStyles = {},
  children,
  outlined = false,
  white = false,
  type = 'button',
  disabled = false,
  onClick = (e) => e,
}: MButtonProps) => {
  const resolveStylesByMod = () => {
    switch (true) {
      case outlined:
        return {
          defaultStyles: {
            border: '1px solid var(--bg-outline)',
            backgroundColor: 'transparent',
          },
          hover: {
            backgroundColor: 'var(--main-darker-color)',
          },
        }
      case white:
        return {
          defaultStyles: {
            border: 'none',
            backgroundColor: 'var(--white)',
            boxShadow: ' 0px 2px 16px 0px #60617029',
          },
          hover: {
            backgroundColor: 'var(--main-darker-color)',
          },
        }
      default:
        return {
          defaultStyles: {
            border: 'none',
            backgroundColor: 'var(--main-color)',
          },
          hover: {
            backgroundColor: 'var(--main-darker-color)',
          },
        }
    }
  }

  const { defaultStyles, hover } = resolveStylesByMod()

  return (
    <Button
      sx={{
        width: ' fit-content',
        font: 'var(--font-p1)',
        fontWeight: '700',
        padding: '10px 18px',
        borderRadius: '55px',
        cursor: 'pointer',
        color: 'var(--text-main)',
        textTransform: 'none',
        '&:hover': {
          ...hover,
        },
        ...inlineStyles,
        ...defaultStyles,
      }}
      onClick={(e) => onClick(e)}
      type={type}
      disabled={disabled}
    >
      {children}
    </Button>
  )
}
