export interface MButtonProps {
  inlineStyles?: {
    [t: string]: string
  }
  children: React.ReactNode
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void
  outlined?: boolean
  white?: boolean
  type?: 'button' | 'submit'
  disabled?: boolean
}
