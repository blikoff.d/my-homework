export interface MModalProps {
  isOpen: boolean
  onClose: () => void
  children: React.ReactElement<any, string | React.JSXElementConstructor<any>>
  isNotice?: boolean
}
