import { Modal } from '@mui/base'

import { CloseIconModal } from 'app/images'

import s from './MModal.module.scss'

import { MModalProps } from './types'
import clsx from 'clsx'

export const MModal: React.FC<MModalProps> = ({
  children,
  isOpen,
  onClose,
  isNotice = false,
}: MModalProps) => {
  return (
    <Modal className={clsx(s.root, isNotice && s.root_notice)} open={isOpen} onClose={onClose}>
      <div className={clsx(s.container, isNotice && s.container_notice)}>
        <CloseIconModal className={s.closeIcon} onClick={onClose} />
        {children}
      </div>
    </Modal>
  )
}
