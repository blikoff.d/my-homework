import { useDispatch } from 'react-redux'
import { useEffect } from 'react'
import { memoize } from 'proxy-memoize'

import { useTypedSelector } from 'shared/hooks/useTypedSelector'

import { setActiveModals } from 'store/slices/activeModals'

import { AuthGuardHocProps } from './types'

export const AuthGuardHoc = ({ children }: AuthGuardHocProps) => {
  const dispatch = useDispatch()

  const { isUserLoggedIn, activeModals } = useTypedSelector(
    memoize((state) => ({
      isUserLoggedIn: state.userInfo.value.user_token !== '',
      activeModals: state.activeModals.value?.[0] || null,
    }))
  )

  useEffect(() => {
    if (!isUserLoggedIn && activeModals !== 'signIn') {
      dispatch(setActiveModals(['signIn']))
      return
    }
  }, [])

  return <>{isUserLoggedIn ? children : null}</>
}
