export interface DataResolverHocProps {
  isLoading: boolean
  error: any
  isError: boolean
  children: React.ReactNode
}
