import { Loader } from 'shared/components/Loader'

import s from './DataResolverHoc.module.scss'
import { DataResolverHocProps } from './types'

export const DataResolverHoc: React.FC<DataResolverHocProps> = ({
  isLoading,
  error,
  isError,
  children,
}: DataResolverHocProps) => {
  const renderContent = () => {
    switch (true) {
      case isLoading:
        return <Loader />
      case isError:
        console.error(error)
        return error
      default:
        return <>{children}</>
    }
  }
  return <div className={s.root}>{renderContent()}</div>
}
