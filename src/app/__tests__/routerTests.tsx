import '@testing-library/jest-dom'
import { screen, waitFor } from '@testing-library/react'

import { setupTestsWrapper } from 'shared/helpers/setupTestWrapper'

describe('Router tests', () => {
  const badRoute = '/some/bad/route'
  setupTestsWrapper({ initialRouterEntries: [badRoute] })

  describe('Check 404 page when no math paths', () => {
    test('landing on a bad page', () => {
      waitFor(() => expect(screen.getByText(/404/i)).toBeInTheDocument())
    })
  })
})
