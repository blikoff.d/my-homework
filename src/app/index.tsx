import { RouterProvider, createBrowserRouter } from 'react-router-dom'

import { routerConfig } from './routerConfig'

import './css/styles.css'

export const App = () => {
  const router = createBrowserRouter(routerConfig)

  return <RouterProvider router={router} />
}
