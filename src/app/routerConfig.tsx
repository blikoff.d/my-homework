import { RouteObject } from 'react-router-dom'

import { AuthGuardHoc } from 'shared/HOCs/AuthGuardHoc/AuthGuardHoc'
import { MainLayout } from 'shared/layouts/MainLayout'

import { FavoriteProductsPage } from 'pages/FavoriteProductsPage'
import { Page404 } from 'pages/Page404'
import { MainPage } from 'pages/MainPage'
import { ProductPage } from 'pages/ProductPage'
import { CatalogPage } from 'pages/CatalogPage'
import { ProfilePage } from 'pages/ProfilePage'
import { CartPage } from 'pages/CartPage'
import { FaqPage } from 'pages/FaqPage'
import { EditProfilePage } from 'pages/EditProfilePage'
import { Review } from 'pages/Review'

export const routerConfig: RouteObject[] = [
  {
    path: '/',
    element: <MainLayout />,
    children: [
      {
        index: true,
        element: <MainPage />,
      },
      {
        path: 'catalog',
        element: (
          <AuthGuardHoc>
            <CatalogPage />
          </AuthGuardHoc>
        ),
      },
      {
        path: 'product/:id',
        element: (
          <AuthGuardHoc>
            <ProductPage />
          </AuthGuardHoc>
        ),
      },
      {
        path: 'review/:id',
        element: (
          <AuthGuardHoc>
            <Review />
          </AuthGuardHoc>
        ),
      },
      {
        path: 'favorites',
        element: (
          <AuthGuardHoc>
            <FavoriteProductsPage />
          </AuthGuardHoc>
        ),
      },
      {
        path: 'cart',
        element: (
          <AuthGuardHoc>
            <CartPage />
          </AuthGuardHoc>
        ),
      },
      {
        path: 'profile',
        element: (
          <AuthGuardHoc>
            <ProfilePage />
          </AuthGuardHoc>
        ),
      },
      {
        path: 'editProfile',
        element: (
          <AuthGuardHoc>
            <EditProfilePage />
          </AuthGuardHoc>
        ),
      },
      {
        path: 'contacts',
        element: <div>Contacts</div>,
      },
      {
        path: 'sales',
        element: (
          <AuthGuardHoc>
            <div>Sales</div>
          </AuthGuardHoc>
        ),
      },
      {
        path: 'feedback',
        element: <div>Feedback</div>,
      },
      {
        path: 'news',
        element: (
          <AuthGuardHoc>
            <div>News</div>
          </AuthGuardHoc>
        ),
      },
      {
        path: 'payments',
        element: <div>Payments</div>,
      },
      {
        path: 'reviews',
        element: (
          <AuthGuardHoc>
            <div>Reviews</div>
          </AuthGuardHoc>
        ),
      },
      {
        path: 'faq',
        element: <FaqPage />,
      },
      {
        path: '*',
        element: <Page404 />,
      },
    ],
  },
]
