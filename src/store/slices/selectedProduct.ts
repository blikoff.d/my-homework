import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { IProductData } from 'shared/types'

export const selectedProductSlice = createSlice({
  name: 'selectedProduct',
  initialState: {
    value: null,
  } as { value: IProductData | null },
  reducers: {
    setSelectedProduct: (state, action: PayloadAction<IProductData | null>) => {
      state.value = action.payload
    },
  },
})

export const { setSelectedProduct } = selectedProductSlice.actions
