import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { IProductData, IWidgetData } from 'shared/types'

const initialState = {
  saleProducts: [],
  newProducts: [],
  recentlyWatchedProducts: [],
}

export const widgetsDataSlice = createSlice({
  name: 'widgetsData',
  initialState: initialState as IWidgetData,
  reducers: {
    setInitialWidgetsData: (state, action: PayloadAction<IWidgetData>) => {
      state = action.payload

      return state
    },
    updateWidgetProduct: (
      state,
      action: PayloadAction<{ product: IProductData; type: keyof typeof initialState }>
    ) => {
      const currentElem = state[action.payload.type].find((product) => {
        return product._id === action.payload.product._id
      })
      const currentIndex = state[action.payload.type].findIndex(
        (product) => product._id === action.payload.product._id
      )

      if (currentElem) {
        state[action.payload.type].splice(currentIndex, 1, action.payload.product)

        return state
      }

      return state
    },

    addRecentlyWatchedProduct: (state, action: PayloadAction<IProductData>) => {
      if (state.recentlyWatchedProducts.find((product) => product._id === action.payload._id))
        return state
      state.recentlyWatchedProducts.push(action.payload)

      return state
    },
  },
})

export const { setInitialWidgetsData, updateWidgetProduct, addRecentlyWatchedProduct } =
  widgetsDataSlice.actions
