import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { IProductData } from 'shared/types'

type CartProduct = {
  product: IProductData
  count: number
}

export const cartProductsSlice = createSlice({
  name: 'cartProducts',
  initialState: [] as Array<CartProduct>,
  reducers: {
    addToCart: (state, action: PayloadAction<CartProduct>) => {
      const currentIndex = state.findIndex(
        (product) => product.product._id === action.payload.product._id
      )
      if (currentIndex !== -1) {
        state[currentIndex].count++
        return state
      }

      state.push(action.payload)
      return state
    },
    removeFromCart: (state, action: PayloadAction<CartProduct>) => {
      const currentIndex = state.findIndex(
        (product) => product.product._id === action.payload.product._id
      )
      if (currentIndex !== -1 && state[currentIndex].count === 1) {
        return state.filter((el) => {
          return el.product._id !== action.payload.product._id
        })
      }

      state[currentIndex].count--

      return state
    },
    removeFullFromCart: (state, action: PayloadAction<CartProduct>) => {
      const currentIndex = state.findIndex(
        (product) => product.product._id === action.payload.product._id
      )

      if (currentIndex !== -1) {
        return state.filter((el) => {
          return el.product._id !== action.payload.product._id
        })
      }

      return state
    },
  },
})

export const { addToCart, removeFromCart, removeFullFromCart } = cartProductsSlice.actions
