import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { IProductData } from 'shared/types'

export const favoriteProductsSlice = createSlice({
  name: 'favoriteProducts',
  initialState: [] as Array<IProductData>,
  reducers: {
    setInitialFavs: (state, action: PayloadAction<Array<IProductData>>) => {
      state = action.payload

      return state
    },
    addToFavoriteProducts: (state, action: PayloadAction<IProductData>) => {
      if (state.find((product) => product._id === action.payload._id)) return state
      state.push(action.payload)

      return state
    },
    removeFromFavoriteProducts: (state, action: PayloadAction<IProductData>) => {
      return state.filter((el) => {
        return el._id !== action.payload._id
      })
    },
  },
})

export const { addToFavoriteProducts, removeFromFavoriteProducts, setInitialFavs } =
  favoriteProductsSlice.actions
