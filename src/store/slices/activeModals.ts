import { PayloadAction, createSlice } from '@reduxjs/toolkit'

export const activeModalsSlice = createSlice({
  name: 'activeModals',
  initialState: {
    value: [],
  } as { value: Array<string> },
  reducers: {
    setActiveModals: (state, action: PayloadAction<Array<string>>) => {
      state.value = action.payload
    },
  },
})

export const { setActiveModals } = activeModalsSlice.actions
