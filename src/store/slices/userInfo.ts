import { PayloadAction, createSlice } from '@reduxjs/toolkit'

type SliceState = {
  user_name: string
  user_email: string
  user_about: string
  user_id: string
  user_token: string
}

const LOCAL_STORAGE_USER_NAME_KEY = 'user_name'
const LOCAL_STORAGE_USER_EMAIL_KEY = 'user_email'
const LOCAL_STORAGE_USER_ABOUT_KEY = 'user_about'
const LOCAL_STORAGE_USER_ID_KEY = 'user_id'
export const LOCAL_STORAGE_USER_TOKEN_KEY = 'user_token'

const defaultUserName = localStorage.getItem(LOCAL_STORAGE_USER_NAME_KEY) || ''
const defaultUserEmail = localStorage.getItem(LOCAL_STORAGE_USER_EMAIL_KEY) || ''
const defaultUserAbout = localStorage.getItem(LOCAL_STORAGE_USER_ABOUT_KEY) || ''
const defaultUserId = localStorage.getItem(LOCAL_STORAGE_USER_ID_KEY) || ''
const defaultUserToken = localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || ''

const initialState: SliceState = {
  user_name: defaultUserName,
  user_email: defaultUserEmail,
  user_about: defaultUserAbout,
  user_id: defaultUserId,
  user_token: defaultUserToken,
}

const setLocalStorageItems = ({ user_about, user_email, user_name, user_id, user_token }) => {
  localStorage.setItem(LOCAL_STORAGE_USER_NAME_KEY, user_name)
  localStorage.setItem(LOCAL_STORAGE_USER_EMAIL_KEY, user_email)
  localStorage.setItem(LOCAL_STORAGE_USER_ABOUT_KEY, user_about)
  localStorage.setItem(LOCAL_STORAGE_USER_ID_KEY, user_id)
  localStorage.setItem(LOCAL_STORAGE_USER_TOKEN_KEY, user_token)
}

export const userInfoSlice = createSlice({
  name: 'userInfo',
  initialState: { value: initialState },
  reducers: {
    setUserInfo: (state, action: PayloadAction<SliceState>) => {
      const { user_about, user_email, user_name, user_id, user_token } = action.payload

      setLocalStorageItems({ user_about, user_email, user_name, user_id, user_token })
      state.value = action.payload
    },
    resetUserInfo: (state) => {
      const { user_name, user_about, user_email, user_id, user_token } = {
        user_name: '',
        user_email: '',
        user_about: '',
        user_id: '',
        user_token: '',
      }

      setLocalStorageItems({ user_about, user_email, user_name, user_id, user_token })

      state.value = { user_name, user_about, user_email, user_id, user_token }
    },
  },
})

export const { setUserInfo, resetUserInfo } = userInfoSlice.actions
