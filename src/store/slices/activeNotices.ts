import { PayloadAction, createSlice } from '@reduxjs/toolkit'

export const activeNoticesSlice = createSlice({
  name: 'activeModals',
  initialState: '',
  reducers: {
    setActiveNotices: (state, action: PayloadAction<string>) => {
      state = action.payload

      return state
    },
  },
})

export const { setActiveNotices } = activeNoticesSlice.actions
