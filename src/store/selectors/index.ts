import { createSelector } from '@reduxjs/toolkit'
import { IWidgetData } from 'shared/types'
import { RootState } from 'store/store'

const selectUserToken = (state: RootState) => state.userInfo.value.user_token
const selectWidgetsData = (state: RootState) => state.widgetsData

const selectActiveModals = (state: RootState) => state.activeModals.value
const selectUserId = (state: RootState) => state.userInfo.value.user_id
const selectCartProducts = (state: RootState) => state.cartProducts

export const isUserLoggedInSelector = createSelector(
  [selectUserToken],
  (userToken) => userToken !== ''
)
export const widgetsDataSelector = (type: keyof IWidgetData) =>
  createSelector([selectWidgetsData], (widgetsData) => widgetsData[type])

export const mainLayoutSelector = createSelector(
  [selectActiveModals, selectUserId, selectUserToken],
  (activeModals, userId, userToken) => ({
    activeModals,
    userId,
    isUserLoggedIn: userToken !== '',
  })
)

export const cartProductsCountSelector = createSelector([selectCartProducts], (cartProducts) =>
  cartProducts.reduce((acc, cur) => (acc += cur.count), 0)
)
