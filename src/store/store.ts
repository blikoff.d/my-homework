import { configureStore } from '@reduxjs/toolkit'

import { mainApi } from './api/mainApi'
import {
  selectedProductSlice,
  activeModalsSlice,
  userInfoSlice,
  activeNoticesSlice,
  favoriteProductsSlice,
  widgetsDataSlice,
  cartProductsSlice,
} from './slices'

export const store = configureStore({
  reducer: {
    selectedProduct: selectedProductSlice.reducer,
    activeModals: activeModalsSlice.reducer,
    userInfo: userInfoSlice.reducer,
    activeNotices: activeNoticesSlice.reducer,
    favoriteProducts: favoriteProductsSlice.reducer,
    widgetsData: widgetsDataSlice.reducer,
    cartProducts: cartProductsSlice.reducer,
    [mainApi.reducerPath]: mainApi.reducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(mainApi.middleware),
})

export type RootState = ReturnType<typeof store.getState>
