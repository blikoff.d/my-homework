import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { IProductData, IProductsData } from 'shared/types'
import { setInitialFavs } from 'store/slices/favoriteProducts'
import { LOCAL_STORAGE_USER_TOKEN_KEY } from 'store/slices/userInfo'
import { setInitialWidgetsData } from 'store/slices/widgetsData'
import { RootState } from 'store/store'

export const mainApi = createApi({
  reducerPath: 'mainApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'https://api.react-learning.ru' }),
  tagTypes: ['Product', 'User', 'Comment'],
  endpoints: (builder) => ({
    //user endpoints
    signIn: builder.mutation<
      { data: { name: string; email: string; about: string; _id: string }; token: string },
      { email: string; password: string }
    >({
      query: (payload) => ({
        url: '/signin',
        method: 'POST',
        body: payload,
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
        },
      }),
    }),
    signUp: builder.mutation<
      { data: { name: string; email: string; about: string; _id: string }; token: string },
      { email: string; password: string; group: string }
    >({
      query: (payload) => ({
        url: '/signup',
        method: 'POST',
        body: payload,
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }),
    }),
    forgotPassword: builder.mutation<
      { data: { name: string; email: string; about: string } },
      { email: string }
    >({
      query: (payload) => ({
        url: '/forgot-password',
        method: 'POST',
        body: payload,
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }),
    }),
    resetPassword: builder.mutation<
      { data: { name: string; email: string; about: string } },
      { tokenFromMail: string; password: string }
    >({
      query: (payload) => ({
        url: `/password-reset/${payload.tokenFromMail}`,
        method: 'PATCH',
        body: {
          password: payload.password,
        },
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
        },
      }),
    }),
    changeUserNameAndAbout: builder.mutation<
      { name: string; email: string; about: string; _id: string },
      { about: string; name: string }
    >({
      query: (payload) => ({
        url: 'users/me',
        method: 'PATCH',
        body: payload,
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
        },
      }),
    }),
    changeUserAvatar: builder.mutation<
      { data: { name: string; email: string; about: string } },
      { avatar: string }
    >({
      query: (payload) => ({
        url: 'users/me/avatar',
        method: 'PATCH',
        body: payload,
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
        },
      }),
    }),

    getUserByToken: builder.query<{ data: { name: string; email: string; about: string } }, void>({
      query: () => ({
        url: '/users/me',
        method: 'GET',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
        },
      }),
    }),

    //products endpoints
    getProducts: builder.mutation<{ products: IProductsData; total: number }, void>({
      query: () => {
        return {
          url: '/products',
          method: 'GET',
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
            authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
          },
        }
      },
      invalidatesTags: ['Comment'],
    }),
    getProductsSearchByParam: builder.mutation<IProductsData, string>({
      query: (payload) => {
        return {
          url: `/products/search/?query=${payload}`,
          method: 'GET',
          headers: {
            'Content-type': 'application/json; charset=UTF-8',
            authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
          },
        }
      },
    }),
    getProductsForWidgests: builder.query<{ products: IProductsData; total: number }, null>({
      query: () => ({
        url: '/products',
        method: 'GET',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
        },
      }),
      async onQueryStarted(id, { dispatch, queryFulfilled, getState }) {
        const state = getState()

        try {
          const { data } = await queryFulfilled

          if (data === undefined) return

          const withNewTag = data.products.filter(
            (product) => product?.tags.length > 0 && product.tags.includes('new')
          )
          const withSaleTag = data.products.filter(
            (product) => product?.tags.length > 0 && product.tags.includes('sale')
          )

          const favorites = data.products.filter((product) =>
            product.likes.includes((state as RootState).userInfo.value.user_id)
          )

          dispatch(setInitialFavs(favorites))
          dispatch(
            setInitialWidgetsData({
              newProducts: withSaleTag,
              saleProducts: withNewTag,
              recentlyWatchedProducts: [],
            })
          )
        } catch (err) {
          console.error('Error fetching post!')
        }
      },
    }),
    getProductById: builder.query<IProductData, string>({
      query: (id) => ({
        url: `/products/${id}`,
        method: 'GET',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
        },
      }),
      providesTags: ['Product', 'Comment'],
    }),
    toggleLikeByParam: builder.mutation<
      IProductData,
      { productId: string; method: 'DELETE' | 'PUT' }
    >({
      query: (payload) => ({
        url: `/products/likes/${payload.productId}`,
        method: payload.method,
        body: payload,
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
        },
      }),
      invalidatesTags: ['Product'],
    }),
    searchProducts: builder.mutation<Array<IProductData>, string>({
      query: (payload) => ({
        url: `/products/search?query=${payload}`,
        method: 'GET',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
        },
      }),
    }),

    addComment: builder.mutation<unknown, { rating: number; comment: string; productId: string }>({
      query: ({ productId, comment, rating }) => ({
        url: `/products/review/${productId}`,
        method: 'POST',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          authorization: localStorage.getItem(LOCAL_STORAGE_USER_TOKEN_KEY) || '',
        },
        body: {
          text: comment,
          rating,
        },
      }),
      invalidatesTags: ['Comment'],
    }),
  }),
})

export const {
  useSignInMutation,
  useGetUserByTokenQuery,
  useChangeUserAvatarMutation,
  useGetProductsForWidgestsQuery,
  useGetProductByIdQuery,
  useToggleLikeByParamMutation,
  useSignUpMutation,
  useForgotPasswordMutation,
  useResetPasswordMutation,
  useChangeUserNameAndAboutMutation,
  useGetProductsMutation,
  useGetProductsSearchByParamMutation,
  useAddCommentMutation,
} = mainApi

// https://nutritious-windflower-27f.notion.site/API-f359059dee014b448955eab8c5b13062 - ссылка на доку апишки
