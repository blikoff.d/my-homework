import { useMemo } from 'react'
import clsx from 'clsx'
import { memoize } from 'proxy-memoize'

import { EmptyResult } from 'shared/components/EmptyResult'
import resolveCountTitle from 'shared/helpers/resolveCountName'

import { ProductFeed } from 'widgets/ProductFeed'
import { MButton } from 'shared/UI/MButton'
import { useTypedSelector } from 'shared/hooks/useTypedSelector'

import s from './CartPage.module.scss'
import { DeliveryIcon } from 'app/images'
import { ProductItem } from './ProductItem'

export const CartPage = () => {
  const { cartProducts } = useTypedSelector(
    memoize((state) => ({
      cartProducts: state.cartProducts,
    }))
  )

  const { cartCount, discount, startPrice, endPrice, numberFormat } = useMemo(() => {
    const { endPrice, discount, cartCount } = cartProducts.reduce(
      (acc, cur) => {
        acc.cartCount += cur.count
        acc.discount += cur.count * (cur.product.discount / 100) * cur.product.price
        acc.endPrice += cur.count * cur.product.price

        return acc
      },
      {
        endPrice: 0,
        discount: 0,
        cartCount: 0,
      }
    )
    const startPrice = endPrice + discount

    const options = { style: 'currency', currency: 'RUB' }
    const numberFormat = new Intl.NumberFormat('ru-RU', options)

    return {
      cartCount,
      discount,
      startPrice,
      endPrice,
      numberFormat,
    }
  }, [JSON.stringify(cartProducts)])

  return (
    <div className={s.root}>
      <div className={s.title}>
        {`${cartCount} ${resolveCountTitle(cartCount, {
          noMany: 'товаров',
          one: 'товар',
          noOne: 'товара',
        })}`}{' '}
        <span className={s['title-default']}>в корзине</span>
      </div>

      {cartCount === 0 ? (
        <EmptyResult
          title="В корзине нет товаров"
          subtitle="Добавьте товар, нажав кнопку «В корзину» в карточке товара"
        />
      ) : (
        <>
          <div className={s.content}>
            <div className={s.cartItems}>
              {cartProducts.map((item) => (
                <ProductItem key={item.product._id} data={item} />
              ))}
            </div>
            <div className={s.orderRegistration}>
              <div className={s.cart}>
                <div className={s.cartTitle}>Ваша корзина</div>
                <div className={s.cartContent}>
                  <div className={clsx(s.subtitle, s['subtitle-secondary'])}>
                    Товары ({cartCount}){' '}
                    <div className={s.price}>{numberFormat.format(startPrice)}</div>
                  </div>
                  {discount ? (
                    <div className={clsx(s.subtitle, s['subtitle-secondary'])}>
                      Скидка{' '}
                      <div className={clsx(s.price, s['price-discount'])}>
                        - {numberFormat.format(discount)}
                      </div>
                    </div>
                  ) : null}
                  <div className={s.separator} />
                  <div className={clsx(s.subtitle, s.commonPrice)}>
                    Общая стоимость{' '}
                    <div className={clsx(s.price, s['price-bolded'])}>
                      {numberFormat.format(endPrice)}
                    </div>
                  </div>
                </div>
                <MButton inlineStyles={{ width: '100%' }}>Оформить заказ</MButton>
              </div>
              <div className={s.delivery}>
                <DeliveryIcon className={s.advantagesIcon} />
                <div className={s.title}>Доставка по всему Миру!</div>
                <div className={s.text}>
                  Доставка курьером — <span className={s['text-bolded']}>от 399 ₽</span>
                </div>
                <div className={s.text}>
                  Доставка в пункт выдачи — <span className={s['text-bolded']}>от 199 ₽</span>
                </div>
              </div>
            </div>
          </div>
          <ProductFeed type="newProducts" />
        </>
      )}
    </div>
  )
}
