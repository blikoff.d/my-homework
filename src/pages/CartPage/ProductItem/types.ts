import { IProductData } from 'shared/types'

export interface ProductItemProps {
  data: {
    product: IProductData
    count: number
  }
}
