import { useDispatch } from 'react-redux'

import { Counter } from 'shared/components/Counter'
import { Price } from 'shared/components/Price'

import { removeFullFromCart } from 'store/slices/cartProducts'
import { TrashIcon } from 'app/images'

import { ProductItemProps } from './types'

import s from './ProductItem.module.scss'

export const ProductItem: React.FC<ProductItemProps> = ({ data }: ProductItemProps) => {
  const dispatch = useDispatch()

  return (
    <div className={s.root}>
      <div className={s.mainInfo}>
        <div className={s.imgWrapper}>
          <img className={s.img} src={data.product.pictures} />
        </div>
        {data.product.name}
      </div>
      <Counter
        product={data}
        inlineStyles={{
          fontSize: 16,
          lineHeight: '20px',
          padding: 5,
          height: 'fit-content',
          width: 92,
        }}
      />
      <div className={s.priceWrap}>
        <Price dataItem={data.product} />
      </div>
      <TrashIcon className={s.icon} onClick={() => dispatch(removeFullFromCart(data))} />
    </div>
  )
}
