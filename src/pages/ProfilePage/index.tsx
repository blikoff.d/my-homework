import { useNavigate } from 'react-router-dom'
import { memoize } from 'proxy-memoize'

import { MButton } from 'shared/UI/MButton'

import s from './ProfilePage.module.scss'
import { useTypedSelector } from 'shared/hooks/useTypedSelector'
import { useDispatch } from 'react-redux'
import { resetUserInfo } from 'store/slices/userInfo'

export const ProfilePage = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const user = useTypedSelector(memoize((state) => state.userInfo.value))

  return (
    <div className={s.root}>
      <div className={s.title}>Профиль</div>
      <div className={s.userInfo}>
        <div className={s.name}>{user.user_name}</div>
        <div className={s.userInfo_secondary}>
          <div>{user.user_about}</div>
          <div>{user.user_email}</div>
        </div>

        <MButton outlined onClick={() => navigate('/editProfile')}>
          Изменить
        </MButton>
      </div>
      <div className={s.exitBtn}>
        <MButton
          outlined
          onClick={() => {
            dispatch(resetUserInfo())
            navigate('/')
          }}
        >
          Выйти
        </MButton>
      </div>
    </div>
  )
}
