import { useMemo, useState } from 'react'
import { Rating } from '@mui/material'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useNavigate } from 'react-router-dom'

import { ContentHeader } from 'shared/components/ContentHeader'
import { Loader } from 'shared/components/Loader'
import { MButton } from 'shared/UI/MButton'

import { useAddCommentMutation, useGetProductByIdQuery } from 'store/api/mainApi'

import s from './Review.module.scss'

import { FormDataType, schema } from './schema'

export const Review = () => {
  const [ratingValue, setRatingValue] = useState<number | null>(null)
  const [isValidRating, setIsValidRating] = useState(true)

  const productIdFromUrl = useMemo(() => {
    const pathname = window.location.pathname
    const [, productId] = pathname.split('/review/')

    return productId
  }, [])

  const navigate = useNavigate()

  const { data: selectedProduct, isLoading } = useGetProductByIdQuery(productIdFromUrl)

  const [addComment] = useAddCommentMutation()

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting, isValid },
  } = useForm({
    resolver: yupResolver(schema),
  })

  const onSubmit = async (dataFromForm: FormDataType) => {
    if (!ratingValue) {
      setIsValidRating(false)
      return
    }

    try {
      const reqObj = { ...dataFromForm, productId: productIdFromUrl, rating: ratingValue }
      const res = await addComment(reqObj).unwrap()

      if (res) {
        console.warn(res)
        navigate(`/product/${selectedProduct?._id}`)
      }
    } catch (e) {
      console.error(e)
    }
  }

  return (
    <div className={s.root}>
      {!selectedProduct || isLoading ? (
        <Loader />
      ) : (
        <>
          <ContentHeader title={`Отзыв от товаре ${selectedProduct.name}`} />
          <form className={s.form} onSubmit={handleSubmit(onSubmit)}>
            <div className={s.commonMark}>
              <div className={s.label}>Общая оценка</div>
              <Rating
                name="rating"
                precision={0.5}
                onChange={(_, newValue) => {
                  setIsValidRating(true)
                  setRatingValue(newValue)
                }}
              />
              <p className={s.invalidInputText}>{!isValidRating ? 'rating is required' : ''}</p>
            </div>
            <div className={s.comment}>
              <div className={s.label}>Комментарий</div>
              <textarea
                className={s.textarea}
                {...register('comment')}
                placeholder="Поделитесь впечатлениями о товаре"
              ></textarea>
              <p className={s.invalidInputText}>{errors.comment?.message}</p>
            </div>
            <MButton type="submit" disabled={!isValid || isSubmitting || !isValidRating}>
              Отправить отзыв
            </MButton>
          </form>
        </>
      )}
    </div>
  )
}
