import * as yup from 'yup'

export const schema = yup
  .object({
    comment: yup.string().required().min(3),
  })
  .required()

export type FormDataType = yup.InferType<typeof schema>
