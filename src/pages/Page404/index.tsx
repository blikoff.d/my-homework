import { Link } from 'react-router-dom'
import s from './Page404.module.scss'

export const Page404 = () => {
  return (
    <div className={s.root}>
      <div className={s.root_title}>404</div>
      <div className={s.root_subtitle}>Sorry, we were unable to find that page</div>
      <div className={s.root_text}>
        Start from{' '}
        <span>
          <Link to="/">home page</Link>
        </span>
      </div>
    </div>
  )
}
