import clsx from 'clsx'
import { useEffect, useMemo, useState } from 'react'
import { useSearchParams } from 'react-router-dom'
import Pagination from '@mui/material/Pagination'
import toSorted from 'core-js-pure/actual/array/to-sorted'
import toReversed from 'core-js-pure/actual/array/to-reversed'

import { ContentHeader } from 'shared/components/ContentHeader'
import { CardItem } from 'shared/components/CardItem'
import { DataResolverHoc } from 'shared/HOCs/DataResolverHoc'
import { ProductFeed } from 'widgets/ProductFeed'
import { useGetProductsMutation, useGetProductsSearchByParamMutation } from 'store/api/mainApi'

import { PRODUCTS_SEARCH_PARAMS_KEY } from 'shared/constants'

import { filters } from './constants'
import s from './CatalogPage.module.scss'

import { IProductsData } from 'shared/types'
import { EmptyResult } from 'shared/components/EmptyResult'

export const CatalogPage = () => {
  const [activeFilter, setActiveFilter] = useState('Популярные')
  const [filteredData, setFilteredData] = useState<IProductsData | null>(null)
  const [srcData, setSrcData] = useState<IProductsData | null>(null)
  const [searchParams] = useSearchParams()
  const [fetchInfo, setFetchInfo] = useState<{
    isError: boolean
    isLoading: boolean
    error: unknown
  }>({
    isError: false,
    isLoading: true,
    error: null,
  })

  const [page, setPage] = useState(1)

  const [getProducts, { isError, isLoading, error }] = useGetProductsMutation()
  const [
    getProductsByParam,
    { isError: isErrorByParam, isLoading: isLoadingByParam, error: errorByParam },
  ] = useGetProductsSearchByParamMutation()

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    if (!srcData) return
    setPage(value)
  }

  const [, searchedQueryValue] = useMemo(() => {
    const params: Array<[string, string]> = []
    searchParams.forEach((value, key) => {
      params.push([key, value])
    })

    const searchedQuery = params.find((el) => el[0] === PRODUCTS_SEARCH_PARAMS_KEY) || ['', '']
    return searchedQuery
  }, [searchParams.toString()])

  useEffect(() => {
    const asyncFc = async () => {
      if (searchedQueryValue) {
        try {
          const data = await getProductsByParam(searchedQueryValue).unwrap()

          setSrcData(data)
          setFetchInfo({
            isError: isErrorByParam,
            isLoading: isLoadingByParam,
            error: errorByParam,
          })
        } catch (e) {
          console.error(e)
        }
      } else {
        try {
          const data = await getProducts().unwrap()

          setSrcData(data.products)
          setFetchInfo({
            isError,
            isLoading,
            error,
          })
        } catch (e) {
          console.error(e)
        }
      }
    }

    asyncFc()
  }, [searchedQueryValue])

  const { productsToShown, countPages } = useMemo(() => {
    if (fetchInfo.isLoading || !srcData)
      return {
        productsToShown: 0,
        countPages: 0,
      }

    const productsToShown = 16
    const countPages = Math.ceil(srcData.length / productsToShown)

    return {
      productsToShown,
      countPages,
    }
  }, [fetchInfo.isLoading])

  const handleFilterForPagination = (data: IProductsData) => {
    return data.filter((el, i) => {
      const minRangeValue = (page - 1) * productsToShown
      const range = [minRangeValue, minRangeValue + productsToShown]
      const isInRange = i >= range[0] && i < range[1]
      if (isInRange) return true
    })
  }

  useEffect(() => {
    if (!srcData) return
    switch (true) {
      case activeFilter === 'Новинки':
        return setFilteredData(handleFilterForPagination(toReversed(srcData)))
      case activeFilter === 'Популярные':
        return setFilteredData(handleFilterForPagination(srcData))
      case activeFilter === 'Сначала дешёвые':
        return setFilteredData(
          handleFilterForPagination(toSorted(srcData, (a, b) => a.price - b.price))
        )
      case activeFilter === 'Сначала дорогие':
        return setFilteredData(
          handleFilterForPagination(toSorted(srcData, (a, b) => b.price - a.price))
        )
      case activeFilter === 'По рейтингу':
        return setFilteredData(
          handleFilterForPagination(
            toSorted(srcData, (a, b) => {
              const aRating =
                a.reviews.reduce((acc, curr) => (acc += curr.rating), 0) / a.reviews.length
              const bRating =
                b.reviews.reduce((acc, curr) => (acc += curr.rating), 0) / b.reviews.length

              return bRating - aRating
            })
          )
        )
      case activeFilter === 'По скидке':
        return setFilteredData(
          handleFilterForPagination(toSorted(srcData, (a, b) => b.discount - a.discount))
        )
      default:
        return setFilteredData(handleFilterForPagination(srcData))
    }
  }, [activeFilter, countPages, page, searchedQueryValue, JSON.stringify(srcData)])

  return (
    <div className={clsx(s.root)}>
      <DataResolverHoc
        isLoading={fetchInfo.isLoading}
        isError={fetchInfo.isError}
        error={fetchInfo.error}
      >
        <>
          {searchedQueryValue && !fetchInfo.isLoading && (
            <div
              className={s.searchInfo}
              style={{ marginBottom: filteredData?.length === 0 ? 0 : '' }}
            >
              По запросу <span className={s['searchInfo-bolded']}>{searchedQueryValue}</span>
              {`найдено ${filteredData?.length} товаров`}
            </div>
          )}

          {!searchedQueryValue && (
            <ContentHeader
              title="Каталог"
              navText="На главную"
              inlineStyles={{ margin: '0 0 20px 0' }}
            />
          )}
          {filteredData?.length === 0 ? (
            <EmptyResult subtitle="Простите, по вашему запросу товаров не найдено." />
          ) : (
            <>
              <div className={s.filters}>
                {filters.map((filter) => (
                  <div
                    className={clsx(
                      s.filters_item,
                      activeFilter === filter && s['filters_item-active']
                    )}
                    key={filter}
                    onClick={() => {
                      setActiveFilter(filter)
                    }}
                  >
                    {filter}
                  </div>
                ))}
              </div>
              <div className={s.products}>
                {filteredData?.map((product) => (
                  <CardItem key={product._id} dataItem={product} setCatalogData={setSrcData} />
                ))}
              </div>
              {searchedQueryValue === '' && (
                <Pagination
                  count={countPages}
                  page={page}
                  onChange={handleChange}
                  sx={{
                    marginBottom: '40px',
                    display: 'flex',
                    justifyContent: 'center',
                    '& .Mui-selected': {
                      backgroundColor: 'var(--main-color)',
                    },
                  }}
                  variant="outlined"
                  size="large"
                />
              )}

              <ProductFeed type="recentlyWatchedProducts" />
            </>
          )}
        </>
      </DataResolverHoc>
    </div>
  )
}
