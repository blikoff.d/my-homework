import { memoize } from 'proxy-memoize'

import { ContentHeader } from 'shared/components/ContentHeader'
import { useTypedSelector } from 'shared/hooks/useTypedSelector'
import { CardItem } from 'shared/components/CardItem'

import s from './FavoriteProductsPage.module.scss'

export const FavoriteProductsPage = () => {
  const favoriteProducts = useTypedSelector(memoize((state) => state.favoriteProducts))

  return (
    <div className={s.root}>
      <ContentHeader title="Избранное" navText="Назад" inlineStyles={{ margin: '0 0 20px 0' }} />
      <div className={s.products}>
        {favoriteProducts.map((product) => (
          <CardItem key={product._id} dataItem={product} />
        ))}
      </div>
    </div>
  )
}
