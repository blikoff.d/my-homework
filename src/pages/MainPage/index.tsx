import clsx from 'clsx'

import { ProductFeed } from 'widgets/ProductFeed'
import { Banner } from 'shared/components/Banner'
import { useTypedSelector } from 'shared/hooks/useTypedSelector'

import { BannerOilIcon, BannerPresentIcon, BannerSetsIcon } from 'app/images'

import s from './MainPage.module.scss'
import { isUserLoggedInSelector } from 'store/selectors'

export const MainPage = () => {
  const isUserLoggedIn = useTypedSelector(isUserLoggedInSelector)

  return (
    <div className={clsx(s.root)}>
      <Banner size="lg" color="Orange">
        <div className={s.banner_content}>
          <div className={s['banner_title-lg']}>Подарок за первый заказ!</div>
          <div className={s['banner_text-lg']}>Легкое говяжье - пластины!</div>
        </div>

        <BannerPresentIcon className={s['banner_icon-lg']} />
      </Banner>
      {isUserLoggedIn && <ProductFeed type="newProducts" />}
      <div className={s.bannersWrap} style={{ marginBottom: !isUserLoggedIn ? 40 : 0 }}>
        <Banner size="md" color="Ocean">
          <div className={s['banner_content-md']}>
            <div className={s['banner_title-md']}>Наборы для дрессировки</div>
            <div className={s['banner_text-md']}>от 900р</div>
          </div>

          <BannerSetsIcon className={s['banner_icon-md']} />
        </Banner>
        <Banner size="md" color="Brown">
          <div className={s['banner_content-md']}>
            <div className={s['banner_title-md']}>Микс масел</div>
            <div className={s['banner_text-md']}>пищевая здоровая натуральная добавка</div>
          </div>

          <BannerOilIcon className={s['banner_icon-md']} />
        </Banner>
      </div>
      {isUserLoggedIn && <ProductFeed type="saleProducts" />}
    </div>
  )
}
