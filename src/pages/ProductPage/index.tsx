import Zoom from 'react-img-zoom'
import { useEffect, useMemo, useState } from 'react'
import clsx from 'clsx'
import { useDispatch } from 'react-redux'
import { memoize } from 'proxy-memoize'
import { Rating } from '@mui/material'
import { useNavigate } from 'react-router-dom'

import { ContentHeader } from 'shared/components/ContentHeader'
import { Label } from 'shared/components/Label'
import { Price } from 'shared/components/Price'
import { MButton } from 'shared/UI/MButton'
import { Counter } from 'shared/components/Counter'
import { Loader } from 'shared/components/Loader'
import { useTypedSelector } from 'shared/hooks/useTypedSelector'
import { parseDate } from 'shared/helpers/parseDate'

import { DeliveryIcon, GuaranteesIcon, HeartIcon, LoopIconPlus } from 'app/images'

import { useGetProductByIdQuery, useToggleLikeByParamMutation } from 'store/api/mainApi'
import { addToFavoriteProducts, removeFromFavoriteProducts } from 'store/slices/favoriteProducts'
import { addToCart } from 'store/slices/cartProducts'

import s from './ProductPage.module.scss'

export const ProductPage = () => {
  const navigate = useNavigate()
  const [customIsFav, setCustomIsFav] = useState(false)
  const [isZoomActive, setIsZoomActive] = useState(false)

  const [toggleLikeByParam] = useToggleLikeByParamMutation()

  const user = useTypedSelector(memoize((state) => state.userInfo.value))
  const dispatch = useDispatch()

  const productIdFromUrl = useMemo(() => {
    const pathname = window.location.pathname
    const [, productId] = pathname.split('/product/')

    return productId
  }, [])

  const { data: selectedProduct, isLoading } = useGetProductByIdQuery(productIdFromUrl)

  useEffect(() => {
    if (!selectedProduct) return
    setCustomIsFav(selectedProduct.likes.includes(user.user_id))
  }, [selectedProduct, user])

  const characters = [
    {
      name: 'Вес',
      text: '1 шт 120-200 грамм',
    },
    {
      name: 'Цена',
      text: `${selectedProduct?.price} ₽ за 100 грамм`,
    },
    {
      name: 'Польза',
      text: [
        'Большое содержание аминокислот и микроэлементов оказывает положительное воздействие на общий обмен веществ собаки.',
        'Способствуют укреплению десен и жевательных мышц.',
        'Развивают зубочелюстной аппарат, отвлекают собаку во время смены зубов.',
        'Имеет цельную волокнистую структуру, при разжевывание получается эффект зубной щетки, лучше всего очищает клыки собак.',
        'Следует учесть высокую калорийность продукта.',
      ],
    },
  ]

  return (
    <>
      {!isLoading && selectedProduct ? (
        <div className={s.root}>
          <ContentHeader title={selectedProduct.name} product={selectedProduct} />
          <div className={s.main}>
            <div className={s['img-big']} onMouseLeave={() => setIsZoomActive(false)}>
              {selectedProduct.discount !== 0 && (
                <div className={s.label}>
                  <Label text={`-${selectedProduct.discount}%`} mode="accent" />
                </div>
              )}
              <div className={clsx(isZoomActive && s.img)}>
                <Zoom
                  img={selectedProduct.pictures}
                  alt="product"
                  zoomScale={isZoomActive ? 3 : 1}
                  width={488}
                  height={488}
                  className="test"
                />
              </div>

              <LoopIconPlus className={s.loopIcon} onClick={() => setIsZoomActive(true)} />
            </div>

            <img src={selectedProduct.pictures} alt="product" className={s['img-small']} />
            <div className={s.preOrder}>
              <Price dataItem={selectedProduct} />
              <div className={s.addCart}>
                <Counter product={{ product: selectedProduct, count: 1 }} />
                <MButton
                  inlineStyles={{ width: '218px', height: '48px' }}
                  onClick={() => {
                    dispatch(addToCart({ product: selectedProduct, count: 1 }))
                  }}
                >
                  В корзину
                </MButton>
              </div>
              <div className={s.toFavorite}>
                <HeartIcon
                  className={clsx(s.favoritesIcon, customIsFav && s['favoritesIcon-active'])}
                  onClick={async () => {
                    if (!customIsFav) {
                      try {
                        const product = await toggleLikeByParam({
                          productId: selectedProduct._id,
                          method: 'PUT',
                        }).unwrap()

                        dispatch(addToFavoriteProducts(product))
                      } catch (e) {
                        console.error(e)
                      }
                    } else {
                      try {
                        const product = await toggleLikeByParam({
                          productId: selectedProduct._id,
                          method: 'DELETE',
                        }).unwrap()

                        dispatch(removeFromFavoriteProducts(product))
                      } catch (e) {
                        console.error(e)
                      }
                    }
                  }}
                />
                В избранное
              </div>
              <div className={s.advantages}>
                <div className={s.delivery}>
                  <DeliveryIcon className={s.advantagesIcon} />
                  <div className={s.title}>Доставка по всему Миру!</div>
                  <div className={s.text}>
                    Доставка курьером — <span className={s['text-bolded']}>от 399 ₽</span>
                  </div>
                  <div className={s.text}>
                    Доставка в пункт выдачи — <span className={s['text-bolded']}>от 199 ₽</span>
                  </div>
                </div>
                <div className={s.guarantees}>
                  <GuaranteesIcon className={s.advantagesIcon} />
                  <div className={s.title}>Гарантия качества</div>
                  <div className={s.text}>
                    Если Вам не понравилось качество нашей продукции, мы вернем деньги, либо сделаем
                    все возможное, чтобы удовлетворить ваши нужды.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={s.description}>
            <div className={s.title}>Описание</div>
            <div className={s.text}>{selectedProduct.description}</div>
          </div>
          <div className={s.characters}>
            <div className={s.title}>Характеристики</div>
            <div className={s.text}>
              {characters.map((character) => (
                <div className={s.characterSection} key={character.name}>
                  <div className={s.characterSubtitle}>{character.name}</div>
                  <div className={s.characterSplit} />
                  <div className={s.characterText}>
                    {Array.isArray(character.text)
                      ? character.text.map((point) => <div key={point}>{point}</div>)
                      : character.text}
                  </div>
                </div>
              ))}
            </div>
          </div>
          <div className={s.reviews} id="reviews">
            <div className={s.title}>Отзывы</div>
            <MButton
              inlineStyles={{ width: '172px', height: '40px' }}
              outlined
              onClick={() => navigate(`/review/${selectedProduct._id}`)}
            >
              Написать отзыв
            </MButton>
          </div>
          <div className={s.reviewsContent}>
            {selectedProduct.reviews.map((review) => (
              <div className={s.reviewItem} key={review._id}>
                <div className={s.nameWrap}>
                  <div className={s.author}>{review.author.name}</div>
                  <div className={s.date}>{parseDate(review.updated_at)}</div>
                </div>

                <Rating name="rating" value={review.rating} precision={0.5} readOnly />

                <div className={s.text}>{review.text}</div>
              </div>
            ))}
          </div>
        </div>
      ) : (
        <Loader />
      )}
    </>
  )
}
