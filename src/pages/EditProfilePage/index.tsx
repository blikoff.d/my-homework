import { useDispatch } from 'react-redux'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import clsx from 'clsx'
import { memoize } from 'proxy-memoize'

import { MButton } from 'shared/UI/MButton'
import { useTypedSelector } from 'shared/hooks/useTypedSelector'
import { ContentHeader } from 'shared/components/ContentHeader'
import { setUserInfo } from 'store/slices/userInfo'
import { setActiveNotices } from 'store/slices/activeNotices'
import { useChangeUserNameAndAboutMutation } from 'store/api/mainApi'

import s from './EditProfilePage.module.scss'

import { FormDataType, schema } from './schema'

export const EditProfilePage = () => {
  const user = useTypedSelector(memoize((state) => state.userInfo.value))

  const dispatch = useDispatch()

  const [changeUserNameAndAbout] = useChangeUserNameAndAboutMutation()

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting, isValid },
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      name: user.user_name,
      about: user.user_about,
    },
  })

  const onSubmit = async (dataFromForm: FormDataType) => {
    try {
      const { name, about } = await changeUserNameAndAbout(dataFromForm).unwrap()

      if (name && about) {
        dispatch(
          setUserInfo({
            user_name: name,
            user_email: user.user_email,
            user_about: about,
            user_id: user.user_id,
            user_token: user.user_token,
          })
        )
        dispatch(setActiveNotices('Профиль успешно обновлен.'))
      }
    } catch (e) {
      dispatch(setActiveNotices('Что-то пошло не так.'))
      console.error(e)
    }
  }

  return (
    <div className={s.root}>
      <ContentHeader
        title="Мои данные"
        navText="Назад"
        inlineStyles={{ margin: '20px 0 20px 0' }}
      />

      <form className={s.form} onSubmit={handleSubmit(onSubmit)}>
        <div className={s.inputGroup}>
          <div className={s.inputWrap}>
            <input
              className={clsx(s.input, errors.name?.message && s['input-failed'])}
              placeholder="Имя"
              {...register('name')}
            />
            <p className={s.invalidInputText}>{errors.name?.message}</p>
          </div>

          <div className={s.inputWrap}>
            <input
              className={clsx(s.input, errors.about?.message && s['input-failed'])}
              placeholder="Пароль"
              {...register('about')}
            />
            <p className={s.invalidInputText}>{errors.about?.message}</p>
          </div>
        </div>

        <MButton outlined type="submit" disabled={!isValid || isSubmitting}>
          Сохранить
        </MButton>
      </form>
    </div>
  )
}
