import * as yup from 'yup'

export const schema = yup
  .object({
    name: yup.string().min(2).required(),
    about: yup.string().min(3).max(48).required(),
  })
  .required()

export type FormDataType = yup.InferType<typeof schema>
