import { useMemo, useState } from 'react'
import { memoize } from 'proxy-memoize'
import { useTypedSelector } from 'shared/hooks/useTypedSelector'

import { CardItem } from 'shared/components/CardItem'
import { NextArrowIcon, PrevArrowIcon } from 'app/images'

import s from './ProductFeed.module.scss'

import { IProductData, IWidgetData } from 'shared/types'
import { titles } from './constants'
import { Loader } from 'shared/components/Loader'

import { widgetsDataSelector } from 'store/selectors'

export const ProductFeed = ({ type }: { type: keyof IWidgetData }) => {
  const productsData = useTypedSelector(memoize(widgetsDataSelector(type)))

  const [selectedPage, setSelectedPage] = useState(0)

  const { dataForSlider, pagesCount } = useMemo(() => {
    const countItemsOnPage = 5

    const getIndexRecentlyWatchedProducts = (i: number, countItemsOnPage: number) =>
      Math.floor(i / countItemsOnPage)

    const data = productsData.reduce((acc: Array<Array<IProductData>>, current, i) => {
      const indexBox = getIndexRecentlyWatchedProducts(i, countItemsOnPage)

      if (!acc?.[indexBox]) {
        acc[indexBox] = [current]
        return acc
      }

      acc[indexBox].push(current)

      return acc
    }, [])

    return {
      dataForSlider: data,
      pagesCount: Math.ceil(productsData.length / countItemsOnPage),
    }
  }, [productsData])

  const handleSwitchPage = ({ isNext }: { isNext: boolean }) => {
    switch (true) {
      case isNext && pagesCount - 1 !== selectedPage:
        setSelectedPage((prev) => prev + 1)
        return
      case isNext && pagesCount - 1 === selectedPage:
        setSelectedPage(0)
        return
      case !isNext && selectedPage === 0:
        setSelectedPage(pagesCount - 1)
        return
      default:
        setSelectedPage((prev) => prev - 1)
    }
  }

  if (type === 'recentlyWatchedProducts' && productsData.length === 0) return null

  return (
    <>
      {productsData.length > 0 ? (
        <div className={s.root}>
          <div className={s.title}>
            {titles[type]}
            {dataForSlider.length > 1 && (
              <div className={s.nav}>
                <div onClick={() => handleSwitchPage({ isNext: false })}>
                  <PrevArrowIcon />
                </div>
                <div onClick={() => handleSwitchPage({ isNext: true })}>
                  <NextArrowIcon />
                </div>
              </div>
            )}
          </div>
          <div className={s.products}>
            {dataForSlider[selectedPage].map((watchedProduct) =>
              type === 'recentlyWatchedProducts' ? (
                <CardItem
                  dataItem={watchedProduct}
                  key={watchedProduct._id}
                  isFull={false}
                  widgetType={type}
                />
              ) : (
                <CardItem dataItem={watchedProduct} key={watchedProduct._id} widgetType={type} />
              )
            )}
          </div>
        </div>
      ) : (
        <div className={s.root}>
          <Loader />
        </div>
      )}
    </>
  )
}
